#include <stdio.h>
#include <stdlib.h>

typedef struct arvore
{
    int info;
    struct arvore *esq;
    struct arvore *dir;
} Arvore;

static Arvore *anterior = NULL;

int arv_bin_check(Arvore *arvore)
{
    if (arvore)
    {
        if (!arv_bin_check(arvore->esq))
            return 0;
        if (anterior != NULL && arvore->info <= anterior->info)
            return 0;
        anterior = arvore;
        return arv_bin_check(arvore->dir);
    }
    return 1;
}

Arvore *inserir(Arvore *a, int v)
{
    if (a == NULL)
    {
        a = (Arvore *)malloc(sizeof(Arvore));
        a->info = v;
        a->esq = a->dir = NULL;
    }
    else if (v < a->info)
    {
        a->esq = inserir(a->esq, v);
    }
    else
    {
        a->dir = inserir(a->dir, v);
    }
    return a;
}

void print(Arvore *a, int spaces)
{
    int i;
    for (i = 0; i < spaces; i++)
        printf(" ");
    if (!a)
    {
        printf("//\n");
        return;
    }

    printf("%d\n", a->info);
    print(a->esq, spaces + 2);
    print(a->dir, spaces + 2);
}

int main()
{
    Arvore *a;

    a = inserir(NULL, 25);
    a = inserir(a, 20);
    a = inserir(a, 10);
    a = inserir(a, 5);
    a = inserir(a, 1);
    a = inserir(a, 8);
    a = inserir(a, 12);
    a = inserir(a, 15);
    a = inserir(a, 22);
    a = inserir(a, 36);
    a = inserir(a, 30);
    a = inserir(a, 28);
    a = inserir(a, 40);
    a = inserir(a, 38);
    a = inserir(a, 48);
    a = inserir(a, 45);
    a = inserir(a, 50);

    if (arv_bin_check(a))
        printf("Árvore é uma Árvore Binária de Busca\n");
    else
        printf("Árvore não é uma Árvore Binária de Busca\n");
    return 0;
}
